console.log('----- Conversão Fahrenheit para Celsius -----')
console.log('')

var tempFar = Math.round(Math.random()*100);
console.log(`Temperatura em Fahrenheit = ${tempFar}°F`);
console.log('')

var tempCel = Math.round(((tempFar - 32)/9)*5);
console.log(`Temperatura em Celsius: ${tempCel}°C`);